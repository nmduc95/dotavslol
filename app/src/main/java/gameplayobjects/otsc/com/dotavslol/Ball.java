package gameplayobjects.otsc.com.dotavslol;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by nmduc on 06-Jan-16.
 */
public class Ball {
    private double x;
    private double y;
    private Bitmap name;
    private int ballsize = 70;
    private int screenWidth;
    private int screenHeight;
    private int bordersize = 30;

    public float radius = ballsize/2;

    public MovingVector MvVt;

    public void getScreenSize(int scrW, int scrH){
        this.screenWidth = scrW;
        this.screenHeight = scrH;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public float getRadius() {
        return radius;
    }

    public Ball(double x, double y, Bitmap name) {
        super();
        this.x = x;
        this.y = y;
        this.name = name;
        this.MvVt = new MovingVector(0.0,0.0);
    }

    public MovingVector getMovingVector() {
        this.MvVt.length = Math.sqrt(this.MvVt.x * this.MvVt.x
                + this.MvVt.y * this.MvVt.y);
        return this.MvVt;
    }

    public void setMovingVector(MovingVector mv1){
        this.MvVt.setX(mv1.getX());
        this.MvVt.setY(mv1.getY());
    }

    public void setMovingVector(int n, MovingVector mv1){
        this.MvVt.setX(mv1.getX() * n);
        this.MvVt.setY(mv1.getY() * n);
    }

    public void setMovingVector(double n, MovingVector mv1){
        this.MvVt.setX(mv1.getX() * n);
        this.MvVt.setY(mv1.getY() * n);
    }

    public void Paint(Canvas canvas) {
        canvas.drawBitmap(name, (float) x - radius, (float) y - radius, null);
    }
    public boolean isMoving(){
        if (this.MvVt.getX() == 0 && this.MvVt.getY() == 0)
            return false;
        else
            return true;
    }

    public void update(){
        this.x += MvVt.x;
        this.y += MvVt.y;

        if (x <= bordersize + radius){
            x = bordersize + radius;
            MvVt.x = Math.abs(MvVt.x);
        }

        if (x >= screenWidth - bordersize - radius){
            x = screenWidth - bordersize - radius;
            MvVt.x = -MvVt.x;
        }

        if (y >= screenHeight - radius - bordersize){
            y = screenHeight - radius - bordersize;
            MvVt.y = -MvVt.y;
        }

        if (y <= bordersize + radius){
            y = bordersize + radius;
            MvVt.y = Math.abs(MvVt.y);
        }
    }
}