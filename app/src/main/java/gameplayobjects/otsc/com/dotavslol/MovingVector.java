package gameplayobjects.otsc.com.dotavslol;

/**
 * Created by nmduc on 12-Jan-16.
 */
public class MovingVector {
    public double y, x;
    public double length;

    public MovingVector() {}

    public MovingVector(double x, double y) {
        this.x = x;
        this.y = y;
        this.length = Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
