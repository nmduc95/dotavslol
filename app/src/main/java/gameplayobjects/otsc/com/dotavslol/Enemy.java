package gameplayobjects.otsc.com.dotavslol;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by nmduc on 06-Jan-16.
 */
public class Enemy {
    private double x;
    private double y;
    private Bitmap name;
    private int playersize = 165;
    public float radius = playersize / 2;
    public MovingVector MvVt;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public float getRadius() {
        return radius;
    }

    public Enemy(double x, Double y, Bitmap name) {
        super();
        this.x = x;
        this.y = y;
        this.name = name;
        this.MvVt = new MovingVector(0.0,0.0);
    }

    public MovingVector getMovingVector() {
        this.MvVt.length = Math.sqrt(this.MvVt.x * this.MvVt.x
                + this.MvVt.y * this.MvVt.y);
        return this.MvVt;
    }

    public void resetVector(){
        this.MvVt = new MovingVector();
    }


    public void setMovingVector(MovingVector mv1){
        this.MvVt = mv1;
    }

    public void Paint(Canvas canvas) {
        canvas.drawBitmap(name, (float) x - radius, (float) y - radius, null);
    }

}
