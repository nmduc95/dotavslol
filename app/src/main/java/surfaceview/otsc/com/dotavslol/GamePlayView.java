package surfaceview.otsc.com.dotavslol;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.ArrayList;

import activities.otsc.com.dotavslol.MainActivity;
import activities.otsc.com.dotavslol.OnlinePlayerActivity;
import activities.otsc.com.dotavslol.R;
import gameplayobjects.otsc.com.dotavslol.Ball;
import gameplayobjects.otsc.com.dotavslol.Enemy;
import gameplayobjects.otsc.com.dotavslol.MovingVector;
import gameplayobjects.otsc.com.dotavslol.Player;
import model.otsc.com.dotavslol.Model;
import sounds.otsc.com.dotavslol.GameSound;
import threads.otsc.com.dotavslol.MainThread;

/**
 * Created by nmduc on 06-Jan-16.
 */
public class GamePlayView extends SurfaceView implements SurfaceHolder.Callback {

    public static double EnemyX = 0, EnemyY = 0;
    public static double BallX = 0, BallY = 0;
    private Display display;
    public static int screenWidth, screenHeight;
    private int playersize;
    private int ballsize;
    private int bordersize;
    private SurfaceHolder sfh;
    private MainThread mainThread;
    Bitmap bmBgGameplay, bmPlayerRed, bmPlayerGreen, bmBall;

    private Player Player;
    public static Enemy Enemy;
    private Ball Ball;

    private double startX, startY;
    private double old_x;
    private double old_y;

    public static double scaleW;
    public static double scaleH;

    private double x =0, y = 0;
    public static boolean player_type=false;

    private boolean inside;

    /**
     * phương thức khởi tạo của GamePlayView
     *
     * @param context
     *            context của ứng dụng
     */
    public GamePlayView(final Context context) {
        super(context);
        mainThread = new MainThread(this);
        sfh = this.getHolder();
        sfh.addCallback(this);

        // get size of the screen
        Activity activity = (Activity) context;
        display = activity.getWindowManager().getDefaultDisplay();
        screenWidth = display.getWidth();
        screenHeight = display.getHeight();

        scaleW = (double) screenWidth/720;
        scaleH = (double) screenHeight/1280;
        double scaleAva = (scaleH + scaleW)/2;
        Toast.makeText(context, screenWidth + " " + screenHeight, Toast.LENGTH_SHORT).show();
        Toast.makeText(context, scaleW + " " + scaleH + " " + scaleAva, Toast.LENGTH_LONG).show();
        playersize =  (int) (165*scaleAva);
        ballsize = (int) (70*scaleAva);
        bordersize = (int) (30*scaleAva);
        Toast.makeText(context, playersize + " " + ballsize + " " + bordersize, Toast.LENGTH_LONG).show();
        Toast.makeText(context, player_type+" ", Toast.LENGTH_LONG).show();

        // Khởi tạo ảnh cho game
        bmBgGameplay = BitmapFactory.decodeResource(getResources(), R.drawable.bg_gameplay);
        bmBall = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        SharedPreferences sp=context.getSharedPreferences(Model.SP_PLAYER_LOGO, Context.MODE_PRIVATE);
        if(sp.getInt(Model.PLAYER_DOTA, 1) == 1){
            bmPlayerRed = BitmapFactory.decodeResource(getResources(), R.drawable.player_dota);
            bmPlayerGreen = BitmapFactory.decodeResource(getResources(), R.drawable.player_lol);
        }
        else {
            bmPlayerRed = BitmapFactory.decodeResource(getResources(), R.drawable.player_lol);
            bmPlayerGreen = BitmapFactory.decodeResource(getResources(), R.drawable.player_dota);
        }

        bmBgGameplay = Bitmap.createScaledBitmap(bmBgGameplay, screenWidth, screenHeight, true);
        bmPlayerGreen = Bitmap.createScaledBitmap(bmPlayerGreen, playersize, playersize , true);
        bmPlayerRed = Bitmap.createScaledBitmap(bmPlayerRed,playersize, playersize , true);
        bmBall = Bitmap.createScaledBitmap(bmBall, ballsize, ballsize, true);

        // generate 2 new players
        Player = new Player(screenWidth / 2, screenHeight - bordersize
                - playersize / 2, bmPlayerRed);
        Enemy = new Enemy(screenWidth / 2, 350.0, bmPlayerGreen);
        Ball = new Ball(screenWidth / 2, screenHeight / 2, bmBall);

        Ball.getScreenSize(screenWidth, screenHeight);

        new Thread(new Runnable() {

            @Override
            public void run() {
                while (true) {
                    if (GamePlayView.player_type) {
                        if (Ball.isMoving())
                            Collision();
                        Ball.update();
                        slowDown();
                        //Collision_Enemy();
                        try {
                            Thread.sleep(10);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                        try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        if (GamePlayView.player_type){
            Thread sendBallCoor = new Thread(new Runnable() {
                @Override
                public void run() {
                    String ballX, ballY = null;
                    ballX = Model.TAG_BALL_X + " " + Double.toString(Ball.getX());
                    byte[] sendX = ballX.getBytes();
                    OnlinePlayerActivity.mChatService.write(sendX);

                    ballY = Model.TAG_BALL_Y + " " + Double.toString(Ball.getY());
                    byte[] sendY = ballY.getBytes();
                    OnlinePlayerActivity.mChatService.write(sendY);
                    Log.i("sendY",sendX.toString()+sendY.toString());

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            sendBallCoor.start();
    } 
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mainThread.setRunning(true);
        if (!mainThread.isAlive())
            mainThread.start();
        Log.i("surface", "surface is created");

        Thread sendEnemyCoor = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    // sending enemy coordinates
                    String enemyX = null;
                    enemyX = Model.TAG_ENEMY_X + " " + Double.toString(x/scaleW);
                    byte[] sendX = enemyX.getBytes();
                    OnlinePlayerActivity.mChatService.write(sendX);
                    try {
                        Thread.sleep(10);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String enemyY = null;
                    enemyY = Model.TAG_ENEMY_Y + " " + Double.toString(y/scaleH);
                    byte[] sendY = enemyY.getBytes();
                    OnlinePlayerActivity.mChatService.write(sendY);
                }
            }
        });
        sendEnemyCoor.start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(getClass().getSimpleName(), "thread check sound touch started");
                while(true){
                    if(Ball.getX() <= bordersize || Ball.getX() >= screenWidth - bordersize
                            || Ball.getY() <= bordersize || Ball.getY() >= screenHeight - bordersize){
                        GameSound.setSoundtrackOn(true);
                        GameSound.playTouch();
                    }
                }
            }
        }).start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mainThread.setRunning(false);
        Log.i("surface", "surface destroyed");
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawBitmap(bmBgGameplay, 0, 0, null);

        Player.Paint(canvas);
        Enemy.Paint(canvas);
        Ball.Paint(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        x = (double) event.getX();
        y = (double) event.getY();

        double endX = 0, endY = 0;
        // bottom
        if (y >= screenHeight - playersize / 2 - bordersize)
            y = screenHeight - playersize / 2 - bordersize;
        // top
        if (y <= screenHeight / 2 + playersize / 2)
            y = screenHeight / 2 + playersize / 2;
        // left
        if (x <= bordersize + playersize / 2)
            x = bordersize + playersize / 2;
        // right
        if (x >= screenWidth - bordersize - playersize / 2)
            x = screenWidth - bordersize - playersize / 2;
        Log.i("log all coordi",Player.getX() + " " + Player.getY() +" "+ Enemy.getX() + " " + Enemy.getY() + " " + Ball.getX() + " " + Ball.getY());

        // set X,Y for player
        Player.setX(x);
        Player.setY(y);

        // set X,Y for Enemy
//		Enemy.setX(screenWidth - x);
//		Enemy.setY(screenHeight - y);

        if (GamePlayView.player_type && event.getAction() == MotionEvent.ACTION_MOVE) {
            if (!isCollide()){
                Log.i("down",startX + " " + startY);
                startX = Player.getX();
                startY = Player.getY();
            }
            if (!Ball.isMoving() && isCollide()) {
                MovingVector move = new MovingVector(Player.getX() - startX, Player.getY() - startY);
                if (move.getX()==0 || move.getY() == 0){
                    Ball.setMovingVector(5,move);
                    Log.i("=0",move.toString());
                }
                else {
                    Ball.setMovingVector(5, simplify(Player.getX() - startX, Player.getY() - startY));
                    Log.i("!0",simplify(move.getX(), move.getY()).toString());
                }
            }

            if (Ball.isMoving())
                Collision();
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {

        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            endX = Player.getX();
            endY = Player.getY();

            if (!Ball.isMoving() && isCollide()) {
                Ball.setMovingVector(5, simplify(endX - startX, endY - startY));
            }
            Player.resetVector();
            Log.i("up", Ball.MvVt.getX() + " " + Ball.MvVt.getY());
        }

        //đoạn này cần code để đưa tọa độ cho đối phương
        return true;
    }

    private MovingVector simplify(double x, double y) {
        MovingVector mvt = new MovingVector(0,0);
        if (x<0 && y<0){
            x = Math.abs(x);
            y = Math.abs(y);
            if (x < y){
                mvt.x = -x / y;
                mvt.y = -1;
            }
            else
            {
                mvt.x = -1;
                mvt.y = -y/x;
            }
        }
        else if (x<0 && y>0){
            x = Math.abs(x);
            if (x < y){
                mvt.x = -x / y;
                mvt.y = 1;
            }
            else
            {
                mvt.x = -1;
                mvt.y = y/x;
            }
        }

        else if (x>0 && y<0){
            y = Math.abs(y);
            if (x < y){
                mvt.x = x / y;
                mvt.y = -1;
            }
            else
            {
                mvt.x = 1;
                mvt.y = -y/x;
            }
        }
        else{
            if (x < y){
                mvt.x = x / y;
                mvt.y = 1;
            }
            else
            {
                mvt.x = 1;
                mvt.y = y/x;
            }
        }
        return mvt;
    }

    private void Collision() {
        double dx = Ball.getX() - Player.getX();
        double dy = Ball.getY() - Player.getY();
        if (!isCollide(dx,dy)) {
            return;
        }

        double angle = Math.atan2(dy, dx);

        MovingVector u1 = Ball.getMovingVector();
        MovingVector u2 = Player.getMovingVector();

        double angle1 = Math.atan2(u1.getY(), u1.getX());
        double angle2 = Math.atan2(u2.getY(), u2.getX());
        double ux1 = u1.getLength() * Math.cos(angle1 - angle);
        double uy1 = u1.getLength() * Math.sin(angle1 - angle);
        double ux2 = u2.getLength() * Math.cos(angle2 - angle);
        double uy2 = u2.getLength() * Math.sin(angle2 - angle);

        double vx1 = ((Ball.getRadius() - Player.getRadius()) * ux1 + (Player
                .getRadius() + Player.getRadius()) * ux2)
                / (Ball.getRadius() + Player.getRadius());
        double vx2 = ((Ball.getRadius() + Ball.getRadius()) * ux1 + (Player
                .getRadius() - Ball.getRadius()) * ux2)
                / (Ball.getRadius() + Player.getRadius());
        double vy1 = uy1;
        double vy2 = uy2;

        MovingVector v1 = new MovingVector();
        MovingVector v2 = new MovingVector();

        v1.setX(Math.cos(angle) * vx1 + Math.cos(angle + Math.PI / 2) * vy1);
        v1.setY(Math.sin(angle) * vx1 + Math.sin(angle + Math.PI / 2) * vy1);
        v2.setX(Math.cos(angle) * vx2 + Math.cos(angle + Math.PI / 2) * vy2);
        v2.setY(Math.sin(angle) * vx2 + Math.sin(angle + Math.PI / 2) * vy2);

//		Log.i("MVVT", v1.x + " " + v1.y + " " + v2.x + " " + v2.y);
        Ball.setMovingVector(1.5, v1);
        Player.resetVector();
        slowDown();

    }

    private void Collision_Enemy(){
        double dx = Ball.getX() - Enemy.getX();
        double dy = Ball.getY() - Enemy.getY();
        if (!isCollide(dx,dy)) {
            return;
        }

        double angle = Math.atan2(dy, dx);

        MovingVector u1 = Ball.getMovingVector();
        MovingVector u2 = Enemy.getMovingVector();

        double angle1 = Math.atan2(u1.getY(), u1.getX());
        double angle2 = Math.atan2(u2.getY(), u2.getX());
        double ux1 = u1.getLength() * Math.cos(angle1 - angle);
        double uy1 = u1.getLength() * Math.sin(angle1 - angle);
        double ux2 = u2.getLength() * Math.cos(angle2 - angle);
        double uy2 = u2.getLength() * Math.sin(angle2 - angle);

        double vx1 = ((Ball.getRadius() - Enemy.getRadius()) * ux1 + (Enemy
                .getRadius() + Enemy.getRadius()) * ux2)
                / (Ball.getRadius() + Enemy.getRadius());
        double vx2 = ((Ball.getRadius() + Ball.getRadius()) * ux1 + (Enemy
                .getRadius() - Ball.getRadius()) * ux2)
                / (Ball.getRadius() + Enemy.getRadius());
        double vy1 = uy1;
        double vy2 = uy2;

        MovingVector v1 = new MovingVector();
        MovingVector v2 = new MovingVector();

        v1.setX(Math.cos(angle) * vx1 + Math.cos(angle + Math.PI / 2) * vy1);
        v1.setY(Math.sin(angle) * vx1 + Math.sin(angle + Math.PI / 2) * vy1);
        v2.setX(Math.cos(angle) * vx2 + Math.cos(angle + Math.PI / 2) * vy2);
        v2.setY(Math.sin(angle) * vx2 + Math.sin(angle + Math.PI / 2) * vy2);

//		Log.i("MVVT", v1.x + " " + v1.y + " " + v2.x + " " + v2.y);
        Ball.setMovingVector(1.5, v1);
        Enemy.resetVector();
        slowDown();
    }

    private boolean isCollide(double dx, double dy) {
        // tong 2 ban kinh
        double dz = Ball.getRadius() + Player.getRadius();

        if ((dx * dx + dy * dy) <= (dz * dz))
            return true;
        else
            return false;
    }

    private boolean isCollide() {
        // khoang cach x giua 2 tam
        double dx = Ball.getX() - Player.getX();
        // khoang cach y giua 2 tam
        double dy = Ball.getY() - Player.getY();
        // tong 2 ban kinh
        double dz = Ball.getRadius() + Player.getRadius();

        if ((dx * dx + dy * dy) <= (dz * dz))
            return true;
        else
            return false;
    }

    private void slowDown() {
        while (Ball.MvVt.getX() > 15 || Ball.MvVt.getY() > 15) {
            Ball.MvVt.setX(Ball.MvVt.getX()/1.5);
            Ball.MvVt.setY(Ball.MvVt.getY()/1.5);
        }
    }

//    private void checkCollisionBall() {
//        double dx = Ball.getX() - Player.getX();
//        double dy = Ball.getY() - Player.getY();
//        if (dx * dx + dy * dy > (Ball.radius + Player.radius) * (Ball.radius + Player.radius)) {
//            return;
//        }
//        double angle = Math.atan2(dy, dx);
//
//        MovingVector u1 = Ball.getMovingVector();
//        MovingVector u2 = Player.getMovingVector();
//
//        double angle1 = Math.atan2(u1.y, u1.x);
//        double angle2 = Math.atan2(u2.y, u2.x);
//        double ux1 = u1.length * Math.cos(angle1 - angle);
//        double uy1 = u1.length * Math.sin(angle1 - angle);
//        double ux2 = u2.length * Math.cos(angle2 - angle);
//        double uy2 = u2.length * Math.sin(angle2 - angle);
//
//        double vx1 = ((Ball.radius - Player.radius) * ux1 + (Player.radius + Player.radius)
//                * ux2)
//                / (Ball.radius + Player.radius);
//        double vx2 = ((Ball.radius + Ball.radius) * ux1 + (Player.radius - Ball.radius)
//                * ux2)
//                / (Ball.radius + Player.radius);
//        double vy1 = uy1;
//        double vy2 = uy2;
//
//        MovingVector v1 = new MovingVector();
//        MovingVector v2 = new MovingVector();
//
//        v1.x = (Math.cos(angle) * vx1 + Math.cos(angle + Math.PI / 2) * vy1);
//        v1.y = (Math.sin(angle) * vx1 + Math.sin(angle + Math.PI / 2) * vy1);
//        v2.x = (Math.cos(angle) * vx2 + Math.cos(angle + Math.PI / 2) * vy2);
//        v2.y = (Math.sin(angle) * vx2 + Math.sin(angle + Math.PI / 2) * vy2);
//
//        Ball.setMovingVector(v1);
//        Player.setMovingVector(v2);
//    }

//    private void checkClickonBall() {
//        if (Player.MvVt.x == 0 && Player.MvVt.y == 0 && Ball.MvVt.x == 0 && Ball.MvVt.y == 0) {
//            double dx = Ball.getX() - Player.getX();
//            double dy = Ball.getY() - Player.getY();
//            Log.i("old", "x: "+old_x+" y: "+old_y);
//            Log.i("new", "x: "+Player.getX()+" y: "+Player.getY());
//            if (dx * dx + dy * dy < (Player.radius + Ball.radius) * (Player.radius + Ball.radius)) {
//                MovingVector vtClickOn = new MovingVector();
//                vtClickOn.x = Player.getX() - old_x;
//                vtClickOn.y = Player.getY() - old_y;
//                Log.i("length", "x: " + vtClickOn.x + "y: " + vtClickOn.y);
//                MovingVector vtPlayer = simplify(vtClickOn.x, vtClickOn.y);
//                Log.i("simplify", "x: " + vtPlayer.x + "y: " + vtPlayer.y);
//                Log.i("old ball vt", Ball.MvVt.x + " " + Ball.MvVt.y);
//                Ball.MvVt.x = 15*vtPlayer.x;
//                Ball.MvVt.y = 15*vtPlayer.y;
//                decreaseVector();
//                Log.i("new ball vt", Ball.MvVt.x + " " + Ball.MvVt.y);
//                inside = true;
//            }
//        }
//        else if (Player.MvVt.x == 0 && Player.MvVt.y == 0 && Ball.MvVt.x != 0 && Ball.MvVt.y != 0) {
//            double dx = Ball.getX() - Player.getX();
//            double dy = Ball.getY() - Player.getY();
//            if (dx * dx + dy * dy <= (Player.radius + Ball.radius) * (Player.radius + Ball.radius)) {
//                MovingVector vtClickOn = new MovingVector();
//                vtClickOn.x = Player.getX() - old_x;
//                vtClickOn.y = Player.getY() - old_y;
//                Log.i("length", "x: " + vtClickOn.x + "y: " + vtClickOn.y);
//                MovingVector vtPlayer = simplify(vtClickOn.x, vtClickOn.y);
//                Log.i("simplify", "x: " + vtPlayer.x + "y: " + vtPlayer.y);
//                Log.i("old ball vt", Ball.MvVt.x + " " + Ball.MvVt.y);
//                Ball.MvVt.x = 20*vtPlayer.x;
//                Ball.MvVt.y = 20*vtPlayer.y;
//                Log.i("new ball vt", Ball.MvVt.x + " " + Ball.MvVt.y);
//                inside = true;
//            }
//        }
//    }
}
