package activities.otsc.com.dotavslol;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Set;


import model.otsc.com.dotavslol.BluetoothChatService;
import model.otsc.com.dotavslol.Model;
import surfaceview.otsc.com.dotavslol.GamePlayView;

public class OnlinePlayerActivity extends Activity {

    private BluetoothAdapter bluetoothAdapter;
    private BroadcastReceiver bluetoothState;
    private BroadcastReceiver discoveryReceiver;
    private ArrayList<BluetoothDevice> arrBluetoohDevice;
    private ArrayList<String> arrPlayersList;
    private ListView lvPlayers;
    private ArrayAdapter<String> arrayAdapter;
    public static BluetoothChatService mChatService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // chế độ full màn hình cho game
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_online_player);
        bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        initComponents();

    }

    /**
     * phương thức khởi tạo các thành phần
     */
    private void initComponents(){
        // register receiver trang thai tu turn on -> turn off cua bluetooh
        bluetoothState = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String prevStateExtra = BluetoothAdapter.EXTRA_PREVIOUS_STATE;
                String stateExtra = BluetoothAdapter.EXTRA_STATE;
                int state = intent.getIntExtra(stateExtra, -1);
                int previousState = intent.getIntExtra(prevStateExtra, -1);

                if(state == BluetoothAdapter.STATE_TURNING_OFF){
                    if(previousState == BluetoothAdapter.STATE_ON){
                        Toast.makeText(getApplicationContext(), "Turn on Bluetooth to play", Toast.LENGTH_SHORT).show();
                        Log.d(getClass().getSimpleName(), "user tắt bluetooh");
                    }
                }
            }
        };
        synchronized (this){
            //dang ky receiver thay doi trang thai cua bluetooth
            registerReceiver(bluetoothState, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));

            arrBluetoohDevice = new ArrayList<>();    // khoi tao danh sach bluetooh players
            arrPlayersList = new ArrayList<>();   // khoi tao danh sach nguoi choi
            lvPlayers = (ListView) findViewById(R.id.lvOnline);
            getPairedList();    //lay danh sach paired

            //khoi tao arrAdapter va set cho listview
            arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrPlayersList);
            lvPlayers.setAdapter(arrayAdapter);
            lvPlayers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View arg1,
                                        int arg2, long arg3) {
                        Log.d("Online Player Activity", "vi tri so" + arg2);
                        try {
                            BluetoothDevice device = arrBluetoohDevice.get(arg2);
                            mChatService.connect(device, false);
                        } catch (Exception e) {
                        }
                    }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(mChatService == null)
            mChatService = new BluetoothChatService(this, mHandler);
    }

    /**
     * The Handler that gets information back from the BluetoothChatService
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Model.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothChatService.STATE_CONNECTED:
                            break;
                        case BluetoothChatService.STATE_CONNECTING:
                            break;
                        case BluetoothChatService.STATE_LISTEN:
                        case BluetoothChatService.STATE_NONE:
                            break;
                    }
                    break;

//                case Model.MESSAGE_WRITE:
//                    byte[] writeBuf = (byte[]) msg.obj;
//                    // construct a string from the buffer
//                    String writeMessage = new String(writeBuf);
//                    break;
                case Model.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Log.i(getClass().getSimpleName(), "message: " + readMessage);

                    // Xu ly cac TAG nhan duoc
                    //tag yeu cau choi
                    if(readMessage.equals(Model.TAG_PLAY)){
                        Intent intent = new Intent(getBaseContext(), GamePlayActivity.class);
                        startActivity(intent);
                    }
                    //tag nhan toa do enemyX
                    if(readMessage.contains(Model.TAG_ENEMY_X)){
                        try {
                            String tmp = readMessage.substring(7);
                            GamePlayView.EnemyX = Double.parseDouble(tmp);
                        } catch (Exception e){Log.e(getClass().getSimpleName(), "loi khi parseDouble enemyX");}
                    }
                    if(readMessage.contains(Model.TAG_ENEMY_Y)) {
                        try {
                            String tmp = readMessage.substring(7);
                            GamePlayView.EnemyY = Double.parseDouble(tmp);
                        } catch (Exception e) {
                            Log.e(getClass().getSimpleName(), "loi khi parseDouble enemyY");
                        }
                    }
                    if(readMessage.contains(Model.TAG_BALL_X)){
                        try {
                            String tmp = readMessage.substring(6);
                            GamePlayView.BallX = Double.parseDouble(tmp);
                        } catch (Exception e){Log.e(getClass().getSimpleName(), "loi khi parseDouble BallX");}
                    }

                    if(readMessage.contains(Model.TAG_BALL_Y)){
                        try {
                            String tmp = readMessage.substring(6);
                            GamePlayView.BallY = Double.parseDouble(tmp);
                        } catch (Exception e){Log.e(getClass().getSimpleName(), "loi khi parseDouble BallY");}
                    }

                    break;
                case Model.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    String mConnectedDeviceName = msg.getData().getString(Model.DEVICE_NAME);
                    Toast.makeText(getBaseContext(), "Connected to "
                                + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case Model.MESSAGE_TOAST:
                    break;
            }
        }
    };


    /**
     * phương thức lấy danh sách các thiết bị paired
     */
    private synchronized void getPairedList(){
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if(pairedDevices.size()>0){
            for (BluetoothDevice device : pairedDevices){
                arrBluetoohDevice.add(device);
                arrPlayersList.add(device.getName() + " - paired");
                if (device.getName() != null){
                Log.d("paired list", device.getName());
            }
            }
        } else Log.d(getClass().getSimpleName(), "paired list = 0");
    }

    private void registerDiscoverable(){
        if (bluetoothAdapter.isEnabled()) {
            if (bluetoothAdapter.startDiscovery()) {
                Log.d(getClass().getSimpleName(), "start Discovery thanh cong");
                // Create a BroadcastReceiver for ACTION_FOUND
                discoveryReceiver = new BroadcastReceiver() {
                    public void onReceive(Context context, final Intent intent) {
                        String action = intent.getAction();
                        // When discovery finds a device
                        if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                            // Get the BluetoothDevice object from the Intent
                            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                            // Add the name and address to an array adapter to show in a ListView
                            try {
                                if(device.getBondState() != BluetoothDevice.BOND_BONDED){
                                    arrBluetoohDevice.add(device);
                                    arrPlayersList.add(device.getName());
                                    arrayAdapter.notifyDataSetChanged();
                                    Log.d(getClass().getSimpleName(), "discoveried " + device.getName());
                                }
                            } catch (Exception e){Log.e(getClass().getSimpleName(), "loi khi discovery thiet bi moi");}
                        }
                    }
                };
                // Register the discoveryReceiver
                IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                registerReceiver(discoveryReceiver, filter);
            }
        }
    }

    public void becomeHost(View v){
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                // Start the Bluetooth chat services
                mChatService.start();
            }
        }
    }

    @Override
    protected void onDestroy() {
        mChatService.stop();
        if(bluetoothState != null)
            unregisterReceiver(bluetoothState);
        if(discoveryReceiver != null)
            unregisterReceiver(discoveryReceiver);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //result code la yeu cau bat che do discoverable
        if(requestCode == Model.REQUEST_ENABLE_DISCOVERABLE){
            if(resultCode == RESULT_OK)
                Log.d(getClass().getSimpleName(), "user đồng ý bật chế độ discoverable");
            if(resultCode == RESULT_CANCELED)
                Log.d(getClass().getSimpleName(), "user không đồng ý bật chế độ discoverable");
        }
    }

    public void refresh(View v){
        if(!bluetoothAdapter.isDiscovering()){
            bluetoothAdapter.startDiscovery();
            Toast.makeText(this, "scanning ...", Toast.LENGTH_SHORT).show();
            Log.d(getClass().getSimpleName(), "scanning ...");
        }
    }

    public void send(View v){
        if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(this, "not connected to any device", Toast.LENGTH_SHORT).show();
            return;
        }
        byte[] send = Model.TAG_PLAY.getBytes();
        mChatService.write(send);
        GamePlayView.player_type = true;
        Intent intent = new Intent(getBaseContext(), GamePlayActivity.class);
        startActivity(intent);
    }

    public void ibtnBack (View v){
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.imagebutton_click));
        this.finish();
    }


}
