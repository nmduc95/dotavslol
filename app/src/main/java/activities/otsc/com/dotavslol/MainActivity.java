package activities.otsc.com.dotavslol;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import model.otsc.com.dotavslol.Model;
import sounds.otsc.com.dotavslol.GameSound;

public class MainActivity extends Activity {
    private SharedPreferences sp;
    private BluetoothAdapter bluetoothAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // chế độ full màn hình cho game
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        GameSound.InitComponents(getApplication());

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            Intent e = new Intent();
            startActivityForResult(enableBtIntent, Model.REQUEST_ENABLE_BT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bluetoothAdapter == null) {
            Toast.makeText(this, "your device does not support Bluetooth", Toast.LENGTH_SHORT).show();
            Log.i(getClass().getSimpleName(), "thiết bị không hỗ trợ Bluetooth");
            this.finish();
        }
        initComponents();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // result code la yeu cau bat bluetooth
        if (requestCode == Model.REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Bluetooth enabled", Toast.LENGTH_SHORT).show();
            }
            if (resultCode == RESULT_CANCELED) {
                Log.i(getClass().getSimpleName(), "user từ chối bật bluetooth");
            }
        }
    }

    private void initComponents() {
        sp = getSharedPreferences(Model.SP_MUSIC_STATUS, MODE_PRIVATE);
        int soundStatus = sp.getInt(Model.SOUND_STATUS, 1);
        if (soundStatus == 1) {
            GameSound.setBackgroundOn(true);
            GameSound.playBackground();
        } else {
            GameSound.setBackgroundOn(false);
            GameSound.stopBackground();
        }
    }

    public void ibtnPlay(View v) {
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.imagebutton_click));
        if (bluetoothAdapter.isEnabled()) {
            Intent intent = new Intent(this, OnlinePlayerActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "turn on bluetooth to play", Toast.LENGTH_SHORT).show();
        }
    }

    public void ibtnSetting(View v) {
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.imagebutton_click));
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
    }


    public void ibtnAbout(View v) {
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.imagebutton_click));
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void ibtnQuit(View v) {
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.imagebutton_click));
        System.exit(0);
    }
}