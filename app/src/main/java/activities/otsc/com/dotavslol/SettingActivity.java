package activities.otsc.com.dotavslol;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import model.otsc.com.dotavslol.Model;
import sounds.otsc.com.dotavslol.GameSound;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener{
    private SharedPreferences sp;
    private BluetoothAdapter bluetoothAdapter;
    private ImageButton ibtnSound;
    private ImageButton ibtnSoundtrack;
    private ImageButton ibtnChangename;
    private ImageButton ibtnLol;
    private ImageButton ibtnDota;
    private TextView tvPlayername;
    private String playerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // chế độ full màn hình cho game
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_setting);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        getViews();
        initComponents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sp = getSharedPreferences(Model.SP_PLAYER_LOGO, MODE_PRIVATE);
        int statusLol = sp.getInt(Model.PLAYER_LOL, 0);
        if(statusLol == 0)
            ibtnLol.setBackgroundResource(R.drawable.setting_player_lol_off);
        else
            ibtnLol.setBackgroundResource(R.drawable.setting_player_lol_on);

        int statusDota = sp.getInt(Model.PLAYER_DOTA, 1);
        if(statusDota == 1)
            ibtnDota.setBackgroundResource(R.drawable.setting_player_dota_on);
        else
            ibtnDota.setBackgroundResource(R.drawable.setting_player_dota_off);

        // check sound status
        SharedPreferences spSound = getSharedPreferences(Model.SP_MUSIC_STATUS, MODE_PRIVATE);
        if(sp.getInt(Model.SOUND_STATUS, 1) == 1)
            ibtnSound.setBackgroundResource(R.drawable.setting_sound_on);
        else
            ibtnSound.setBackgroundResource(R.drawable.setting_sound_off);
    }

    private void getViews(){
        // get views by ID
        ibtnSound = (ImageButton)findViewById(R.id.ibtnSound);
        ibtnSoundtrack = (ImageButton)findViewById(R.id.ibtnSoundtrack);
        ibtnChangename = (ImageButton)findViewById(R.id.ibtnChangeName);
        ibtnLol = (ImageButton)findViewById(R.id.ibtnLol);
        ibtnDota = (ImageButton)findViewById(R.id.ibtnDota);
        tvPlayername = (TextView)findViewById(R.id.tvPlayername);

        // set onClick event
        ibtnSound.setOnClickListener(this);
        ibtnSoundtrack.setOnClickListener(this);
        ibtnChangename.setOnClickListener(this);
        ibtnLol.setOnClickListener(this);
        ibtnDota.setOnClickListener(this);
    }

    private void initComponents(){
        sp = getSharedPreferences(Model.SP_PLAYER_INFOR, MODE_PRIVATE);
        if(bluetoothAdapter.isEnabled()) {
            playerName = bluetoothAdapter.getName();
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(Model.PLAYER_NAME, playerName);
            if (!editor.commit())
                Log.e(getClass().getSimpleName(), "lỗi khi lưu player name vào bộ nhớ cache");
        }
        playerName = sp.getString(Model.PLAYER_NAME, "player");
        tvPlayername.setText(playerName);
    }

    public void ibtnBack(View v){
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.imagebutton_click));
        this.finish();
    }

    @Override
    public void onClick(View v) {
        // changename event
        if(v == ibtnChangename){
            v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.imagebutton_click));
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Enter username");
            // Set up the input
            final EditText input = new EditText(this);
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);
            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(!input.getText().toString().equals("") && bluetoothAdapter.isEnabled()) {
                        playerName = input.getText().toString();
                        SharedPreferences.Editor edit = sp.edit();
                        edit.putString(Model.PLAYER_NAME, playerName);
                        edit.commit();
                        bluetoothAdapter.setName(playerName);
                        tvPlayername.setText(playerName);
                    } else Toast.makeText(getBaseContext(), "please turn on bluetooth or enter something to change", Toast.LENGTH_SHORT).show();
                }
            });
            builder.show();
        }

        // change sound event
        if(v == ibtnSound) {
            SharedPreferences spTemp = getSharedPreferences(Model.SP_MUSIC_STATUS, MODE_PRIVATE);
            // if sound is on
            if (GameSound.isBackgroundOn()) {
                SharedPreferences.Editor editor = spTemp.edit();
                editor.putInt(Model.SOUND_STATUS, 0);
                editor.commit();
                ibtnSound.setBackgroundResource(R.drawable.setting_sound_off);
                GameSound.setBackgroundOn(false);
                GameSound.stopBackground();
            }
            else {
                SharedPreferences.Editor editor = spTemp.edit();
                editor.putInt(Model.SOUND_STATUS, 1);
                editor.commit();
                ibtnSound.setBackgroundResource(R.drawable.setting_sound_on);
                GameSound.setBackgroundOn(true);
                GameSound.playBackground();
            }
        }

        // change lol logo
        if(v == ibtnLol){
            SharedPreferences spTemp = getSharedPreferences(Model.SP_PLAYER_LOGO, MODE_PRIVATE);
            SharedPreferences.Editor editor = spTemp.edit();
            editor.putInt(Model.PLAYER_LOL, 1);
            editor.putInt(Model.PLAYER_DOTA, 0);
            editor.commit();
            ibtnLol.setBackgroundResource(R.drawable.setting_player_lol_on);
            ibtnDota.setBackgroundResource(R.drawable.setting_player_dota_off);
        }

        //change logo dota
        if(v == ibtnDota){
            SharedPreferences spTemp = getSharedPreferences(Model.SP_PLAYER_LOGO, MODE_PRIVATE);
            SharedPreferences.Editor editor = spTemp.edit();
            editor.putInt(Model.PLAYER_DOTA, 1);
            editor.putInt(Model.PLAYER_LOL, 0);
            editor.commit();
            ibtnDota.setBackgroundResource(R.drawable.setting_player_dota_on);
            ibtnLol.setBackgroundResource(R.drawable.setting_player_lol_off);
        }
    }
}
