package sounds.otsc.com.dotavslol;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import java.io.IOException;

import activities.otsc.com.dotavslol.R;

/**
 * Created by nmduc on 06-Jan-16.
 */
public class GameSound {

    public static void InitComponents(Context ctx){
        context=ctx;
        mpBackground = MediaPlayer.create(context, R.raw.background_music);
        mpBackground.setLooping(true);
    }
    public static boolean isBackgroundOn() {
        return backgroundOn;
    }

    public static void setBackgroundOn(boolean backgroundOn) {
        GameSound.backgroundOn = backgroundOn;
    }

    public static void playBackground(){
        if(isBackgroundOn()){
            if(!mpBackground.isPlaying()) {
                mpBackground.start();
            }
        }
    }

    public static void stopBackground(){
        if(!isBackgroundOn()){
            if(mpBackground.isPlaying()) {
                mpBackground.stop();
                try {
                    mpBackground.prepare();
                } catch (IOException e) {
                    Log.e("prepare", "loi khi goi prepare()");
                }
            }
        }
    }

    public static void playTouch(){
        if(soundtrackOn) {
            MediaPlayer.create(context, R.raw.touch).start();
            Log.d("GameSound", "sound touch is played");
        }
    }

    public static boolean isSoundtrackOn() {
        return soundtrackOn;
    }

    public static void setSoundtrackOn(boolean soundtrackOn) {
        GameSound.soundtrackOn = soundtrackOn;
    }

    private static boolean backgroundOn=false;
    private static boolean soundtrackOn = false;
    private static MediaPlayer mpBackground;
    private static Context context;
}
