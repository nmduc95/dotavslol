package model.otsc.com.dotavslol;

/**
 * Created by nmduc on 07-Jan-16.
 */
public interface Model {
    public static final int ORIGINAL_WIDTH = 768 ;
    public static final int ORIGINAL_HEIGHT = 1184;

    public static final int REQUEST_ENABLE_BT = 1;    // request code bat bluetooh
    public static final int REQUEST_ENABLE_DISCOVERABLE = 2;   // request code bat che do discoverable
    public static final String SP_PLAYER_INFOR = "sp_player_infor";   // ten SharedPreferences chua thong tin nguoi choi
    public static final String PLAYER_NAME = "player_name";   // ten nguoi choi
    public static final String SP_MUSIC_STATUS = "music_status"; // SharedPreferences chua tinh trang cua am thanh game
    public static final String SOUND_STATUS = "sound_status";     // tinh trang cua nhac nen
    public static final String SOUNDTRACK_STATUS = "soundtrack_status";       // tinh trang cua soundtrack
    public static final String SP_PLAYER_LOGO = "sp_player_logo";   //SharedPreferences chua tinh trang hien tai
    public static final String PLAYER_LOL = "player_lol";
    public static final String PLAYER_DOTA = "player_dota";

    public static final int SUCCESS_CONNECT = 0;

    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Tag game play to transfer data
    public static final String TAG_PLAY = "play";
    public static final String TAG_ENEMY_X = "enemyX";
    public static final String TAG_ENEMY_Y = "enemyY";

    public static final String TAG_BALL_X = "ballX";
    public static final String TAG_BALL_Y = "ballY";
}
