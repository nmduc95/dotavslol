package threads.otsc.com.dotavslol;

import android.graphics.Canvas;
import android.util.Log;

import surfaceview.otsc.com.dotavslol.GamePlayView;

/**
 * Created by nmduc on 06-Jan-16.
 */
public class MainThread extends Thread {
    private GamePlayView gpv;
    private volatile boolean isRunning=false, isPaused;

    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }

    public void setPaused(int i) {
        synchronized(gpv.getHolder()) {
            if(i==0)
                isPaused=false;
            else
                isPaused=true;
        }
    }

    public MainThread(GamePlayView gpv){
        this.gpv=gpv;
    }

    @Override
    public void run() {
        while(isRunning){
            Canvas c=null;
            try {
                c=gpv.getHolder().lockCanvas();

                synchronized (gpv.getHolder()) {
                    GamePlayView.Enemy.setX(GamePlayView.screenWidth - GamePlayView.EnemyX*GamePlayView.scaleW);
                    GamePlayView.Enemy.setY(GamePlayView.screenHeight - GamePlayView.EnemyY*GamePlayView.scaleH);
                    gpv.draw(c);
                }
            } catch (Exception e) {
            } finally {
                if(c!=null)
                    gpv.getHolder().unlockCanvasAndPost(c);
            }

            try {
                this.sleep(10);
            } catch (Exception e) {
            }
        }
    }
}
